![picture](http://developer.postgresql.org/~josh/graphics/logos/elephant-64.png")
# Parent poms for Datometry JDBC driver

This repository includes maven parent poms that are used by Datometry JDBC driver.

## Info

In case base dependency (e.g. `maven-compiler-plugin` version) needs to be changed, a relevant change to the `pgjdbc-parent-poms` repository should
be made and this new version should be used in main `pgjdbc` repository.

## Build requirements

In order to build the set of parent poms, you will need the following tools:

- A git client
- A recent version of Maven (3.x)
- A JDK (any should work)

## Checking out the source code

The PgJDBC project uses git for version control. You can check out the current code by running:

    git clone https://bitbucket.org/datometry/jdbc.git

This will create a jdbc-parent-poms directory containing the checked-out source code.

## Installing parent poms to local repository

After checking out the code you can install new poms to your local repository:

    mvn install

## Releasing a snapshot version

Git repository typically contains -SNAPSHOT versions, so you can use the following command:

    mvn deploy

## Releasing a new version

Procedure:

To commit updates to version in `pom.xml` files and create a tag, issue:

    mvn release:clean release:prepare

To stage the version to maven central, issue:

    mvn release:perform

This will open staging repository for smoke testing access at https://oss.sonatype.org/content/repositories/orgpostgresql-1082/

If staged artifacts look fine, release it

    mvn nexus-staging:release -DstagingRepositoryId=orgpostgresql-1082

## Dependencies

`jdbc-parent-poms` has little to no dependencies itself. It just lists defaults to be used by core `dtmjdbc` project.

